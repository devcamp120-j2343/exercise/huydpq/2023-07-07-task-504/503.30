import { Product } from "./product.js";
class Movie extends Product {
    constructor(director){
        this.director = director;
    }
}

export {Movie}