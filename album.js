import { Product } from "./product.js";
class Album extends Product {
    constructor(paramName,paramPrice, paramNumber,artist){
        super(paramName,paramPrice,paramNumber)
        this.artist = artist;
    }
    show(){
        return `Name: ${this.name}, Price: ${this.price}, NumberOfcopies: ${this.numberOfcopies} , Artist: ${this,this.artist}`;
    }
}
export {Album}