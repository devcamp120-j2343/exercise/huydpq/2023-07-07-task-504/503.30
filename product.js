class Product  {
    constructor(name , price , numberOfcopies){
        this.name = name;
        this.price = price;
        this. numberOfcopies = numberOfcopies;
    }

    sellCopies() {
        return `Name: ${this.name}, Price: ${this.price}, NumberOfcopies: ${this.numberOfcopies}`;
    }
}
export {Product}