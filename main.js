import { Product } from "./product.js";
import { Album } from "./album.js";
import { Movie } from "./movie.js";

var product1 = new Product("Huy", 1.000, 10);
console.log(product1);
console.log(product1.sellCopies());

var album = new Album("lan", 9.000, 18,"aaa")
console.log(album);
console.log(album.show());
